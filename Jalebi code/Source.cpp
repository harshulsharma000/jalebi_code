/*
												Jalebi Version 2.0
*/

#include <iostream>
#include <Windows.h>

using namespace std;

void modify(int& r, int& c, int& nature, int& change, int **a, int& ext , int &value)
{
		while (1)
		{
			a[r][c] = value; 
			value++;	
			
			/*
				This loop breaks if the current element is just before the extreme one as it does not set the extreme element and leaves it to 
				be set by next coloumn or row
			*/
			if ((change + nature) == ext) 
			{
				change += nature; //But it places the changing value to the extreme
				break;
			}
			change += nature; //If nature is positive then c will increase and if negative then it will decrease, perfectly suits the case
		}
}

int main()
{
	int n = 0, value = 1, status = 0, min = 0, max;   
	int **a, r = 0, c = 0, nature = 1, *change = &c, *ext = &max;  //Nature for first cycle is +ve and extremity is max length
	cout << "Enter the size of jalebi" << endl;
	cin >> n;
	cout << endl << endl;
	max = n-1; // The max value must be 1 less than size of matrix (array's rule)
	
	//Dynamically allocating an array
	
	a = new int*[n];
	for (int i = 0; i < n; i++)
	{
		a[i] = new int[n];
	}


	//Modification Code which modifies each line of our 2-d array
	
	while (value != (n*n)) //A loop that check if all the required elements are filled up
	{
		modify(r, c, nature, *change, a, *ext ,value); //Modifies one row or coloumn for the matrix
		status++; //One cycle complete
		change = (change == &r) ? &c : &r;  //changing element shift from row to col 
		if (status % 2 == 0 && status != 0) //After 2 cycles nature needs to be change and the extreme value too
		{
			nature *= -1;
			ext = (nature == 1) ? &max : &min;
		}

		if (status == 4)  //After completion of four cycles one round completes, now reset and adjust for next round
		{
			status = 0;
			max--;
			min++;
			r++;
			c++;
		}

	}

	a[r][c] = value; //Placement of last uninserted value

	//Displays finally arranged matrix according to the pattern
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (a[i][j] < 10)
				cout << " ";
			cout << a[i][j] << " ";
			
		}
		cout << endl;
	}
	
	//Deallocation and cleanup
	for (int i = 0; i < n; i++)
	{
		delete[]  a[i];
	}
	delete[] a;
	
	system("pause");
	return 0;
}